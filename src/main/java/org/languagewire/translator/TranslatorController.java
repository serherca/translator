package org.languagewire.translator;


import org.languagewire.translator.model.Language;
import org.languagewire.translator.service.TranslatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class TranslatorController {
    public final String SOURCE_INVALID = "Source language value is not allowed";
    public final String TARGET_INVALID = "Target language value is not allowed";

    @Autowired
    TranslatorService translator;

    @GetMapping(value="/translate")
    public String translate(@RequestParam(required = false, defaultValue = "auto") String source,
                            @RequestParam(required = false, defaultValue = "auto") String target,
                            @RequestParam(required = true) String text) {
        if (!Language.contains(source)) return SOURCE_INVALID;
        if (!Language.contains(target)) return TARGET_INVALID;
        if (!text.isEmpty()) {
            try {
                return translator.translate(source, target, text);
            } catch (IOException e){
                return "Process has failed because of: " + e.getMessage();
            }
        }
        else return "";
    }
}
