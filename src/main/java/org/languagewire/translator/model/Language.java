package org.languagewire.translator.model;

import java.util.stream.Stream;

public enum Language {
    AUTO("Automatic", "auto"),
    ENGLISH("English", "en"),
    FRENCH("French", "fr"),
    GERMAN("German", "de");

    public final String value;
    public final String key;

    private Language(String value, String key){
        this.value = value;
        this.key = key;
    }

    public static boolean contains(String language){
        return Stream.of(Language.values()).anyMatch(lang -> lang.value.equals(language));
    }
}
