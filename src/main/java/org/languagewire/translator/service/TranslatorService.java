package org.languagewire.translator.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Service
public class TranslatorService {
    private final String URL_BASE = "https://translate.google.cn";
    private final String GOOGLE_TTS_RPC = "MkEWBc";

    public String translate(String source, String target, String text) throws IOException {
        final String URL_SERVICE = URL_BASE + "/_/TranslateWebserverUi/data/batchexecute";
        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost request = new HttpPost(URL_SERVICE);
        addHeader(request);
        StringEntity entity = new StringEntity(formatRequestParams(source, target, text));
        entity.setContentType("application/x-www-form-urlencoded;charset=utf-8");
        request.setEntity(entity);

        CloseableHttpResponse response = (CloseableHttpResponse) httpClient.execute(request);
        // TODO parse response
        if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()){
            return decodeResponse(response);
        } else
            new IOException("Translate Service Error: Http code " + response.getStatusLine().getStatusCode());

        return "";
    }

    private String decodeResponse(CloseableHttpResponse response) throws IOException {
        try (response) {
            HttpEntity entity = response.getEntity();
            if (entity != null && entity.isStreaming()) {
                String responseStr = EntityUtils.toString(entity);
                String[] lines = responseStr.split(System.getProperty("\n"));
                for (String line : lines) {
                    if (line.contains(GOOGLE_TTS_RPC)){
                        line = line + "]";
                        
                    }
                }
            }
        }

        return "";
    }

    public String formatRequestParams(String source, String target, String text) throws UnsupportedEncodingException {
        String freq = "[[[\"" + GOOGLE_TTS_RPC + "\",\"[[\\\"" + text.trim() + "\\\",\\\"" + source + "\\\",\\\"" + target + "\\\",true],[1]]\",null,\"generic\"]]]";
        String freqEncoded = URLEncoder.encode(freq, StandardCharsets.UTF_8.toString()).replace("+","%20");
        return "f.req=" + freqEncoded + "&";
    }

    private void addHeader(HttpPost request) {
        request.addHeader("Referer", URL_BASE);
        request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) \n" +
                "AppleWebKit/537.36 (KHTML, like Gecko) \n" +
                "Chrome/47.0.2526.106 Safari/537.36");
        request.addHeader("content-type", "application/x-www-form-urlencoded;charset=utf-8");
    }
}
