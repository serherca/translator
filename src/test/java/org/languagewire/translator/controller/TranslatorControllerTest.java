package org.languagewire.translator.controller;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.languagewire.translator.TranslatorController;
import org.languagewire.translator.service.TranslatorService;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TranslatorControllerTest {

    @Autowired
    TranslatorController controller;

    @Mock
    TranslatorService service;

    private String source;
    private String target;
    private String text;
    private String expectedValue;

    @BeforeEach
    public void init() throws IOException{
        this.source = "English";
        this.target = "French";
        this.text = "Hello World";
        this.expectedValue = "Bonjour le monde";
        Mockito.when(service.translate(any(), any(), any())).thenReturn(expectedValue);
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From English into French of sentence 'Hello World' Then result is 'Bonjour le monde'.")
    public void translateHappyPath() {
        String result = controller.translate(source, target, text);
        Assert.assertEquals(expectedValue, result);
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From <empty> into French of sentence 'Hello World' Then result is 'Bonjour le monde'.")
    public void translateMissingSource(){
        String result = controller.translate(null, target, text);
        Assert.assertEquals(expectedValue, result);
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From English into <empty> of sentence 'Hello World' Then result is 'Bonjour le monde'.")
    public void translateMissingTarget(){
        String result = controller.translate(source, null, text);
        Assert.assertEquals(expectedValue, result);
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From English into French of sentence '' Then result is ''.")
    public void translateMissingText(){
        String result = controller.translate(source, target, null);
        Assert.assertEquals("", result);
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From Spanish into French of sentence 'Hello World' Then result is 'Source language value is not allowed'.")
    public void translateInvalidSource(){
        String result = controller.translate("Spanish", target, text);
        Assert.assertEquals(controller.SOURCE_INVALID, result);
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From English into Spanish of sentence 'Hello World' Then result is 'Target language value is not allowed'.")
    public void translateInvalidTarget(){
        String result = controller.translate(source, "Spanish", text);
        Assert.assertEquals(controller.TARGET_INVALID, result);
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From English into French of sentence 'Hello World' Then result is ERROR.")
    public void translateError() throws IOException{
        String msg = "ERROR";
        Mockito.when(service.translate(any(), any(), any())).thenThrow(new IOException(msg));

        String result = controller.translate(source, target, text);
        Assert.assertEquals("Process has failed because of: " + msg, result);
    }

}
