package org.languagewire.translator.service;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class TranslatorServiceTest {

    @Mock
    TranslatorService service;


    private String source;
    private String target;
    private String text;
    private String expectedValue;

    @BeforeEach
    public void init() {
        this.source = "English";
        this.target = "French";
        this.text = "Hello World";
        this.expectedValue = "Bonjour le monde";
    }

    @Test
    @DisplayName(value="Given Ruvan has called to the service When executing a translation From English into French of sentence 'Hello World' Then result is 'Bonjour le monde'.")
    public void translateHappyPath() throws IOException {
        Mockito.when(service.translate(any(),any(),any())).thenCallRealMethod();
        Mockito.when(service.formatRequestParams(any(),any(),any())).thenCallRealMethod();

        String result = service.translate(source, target, text);
        Assert.assertEquals(expectedValue, result);
    }

    @Test
    @DisplayName(value="Given testing formatRequestParams method When executing a call Then result must match with the formatted.")
    public void formatRequestParamsTest() throws IOException {
        Mockito.when(service.formatRequestParams(any(),any(),any())).thenCallRealMethod();

        String result = service.formatRequestParams(source, target, text);
        String expectedParams = "f.req=%5B%5B%5B%22MkEWBc%22%2C%22%5B%5B%5C%22Hello%20World%5C%22%2C%5C%22English%5C%22%2C%5C%22French%5C%22%2Ctrue%5D%2C%5B1%5D%5D%22%2Cnull%2C%22generic%22%5D%5D%5D&";

        Assert.assertEquals(expectedParams, result);
    }

    @Test
    @DisplayName(value="Given testing formatRequestParams method When executing a call Then result must match with the formatted.")
    public void formatRequestParamsFailsTest() throws IOException {
        String msg = "ERROR";
        Mockito.when(service.formatRequestParams(any(),any(),any())).thenThrow(new UnsupportedEncodingException(msg));
        Mockito.when(service.translate(any(),any(),any())).thenCallRealMethod();

        try {
            service.translate(source, target, text);
            Assert.fail();
        } catch (UnsupportedEncodingException e){
            Assert.assertEquals(msg, e.getMessage());
        }
    }
}
